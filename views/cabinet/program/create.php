<?php
use app\models\Program;

/* @var $this yii\web\View */
/* @var $model Program */

$this->title = 'Создать программу';
?>
<article class="container">
	<div class="page">
		<h1><?= $this->title ?></h1>
		<?= $this->render('_form', [
			'model' => $model,
		]) ?>
	</div>
</article>