<?php
use app\models\Program;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Program */
?>
<div class="form">
	<?php $form = ActiveForm::begin([
		'id' => 'post-form',
		'fieldConfig' => [
			'template' => '<div class="form-row">{label}{input}{error}</div>',
		],
		'enableClientValidation' => true,
	]); ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

	<?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
		'language' => 'ru',
		'dateFormat' => 'dd.MM.yyyy',
		'clientOptions' => [
			'yearRange' => 'c-2:c+2',
			'changeMonth' => true,
			'changeYear' => true,
		],
		'options' => [
			'class' => 'form-control',
			'style' => 'z-index: 2; position: relative;',
		],
	]) ?>


    <?= $form->field($model, 'duration') ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => 'btn btn-success btn-block btn-lg']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>