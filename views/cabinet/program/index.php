<?php
use app\models\Program;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/** @var \app\models\search\TeamSearch $model */
/** @var $this yii\web\View */
/** @var $dataProvider \yii\data\DataProviderInterface */
$this->title = 'Программы';

?>

<article class="container">
    <div class="page">
        <h1>Программы</h1>
        <div class="form-group">
            <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/program/create']) ?>" role="button" class="btn btn-primary">
                Создать программу
            </a>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'search-form',
                    'fieldConfig' => [
                        'template' => '{input}',
                    ],
                    'enableClientValidation' => true,
                    'options' => [
                        'class' => 'form-inline',
                    ]
                ]); ?>
                <?= $form->field($model, 'name')->textInput([
                    'placeholder' => $model->getAttributeLabel('name')
                ]) ?>
                <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'clientOptions' => [
                        'yearRange' => 'c-2:c+2',
                        'changeMonth' => true,
                        'changeYear' => true,
                    ],
                    'options' => [
                        'placeholder' => 'Дата старта',
                        'class' => 'form-control',
                        'style' => 'z-index: 2; position: relative;',
                    ],
                ]) ?>
                <?= $form->field($model, 'duration')->textInput([
                    'placeholder' => $model->getAttributeLabel('duration')
                ]) ?>
                <button type="submit" class="btn btn-success">Искать</button>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'label' => 'Программа',
                    'attribute' => 'name',
                ],
                [
                    'label' => 'Дата старта',
                    'attribute' => 'start_date',
                    'format' => ['date', 'php:d.m.Y'],
                ],
                [
                    'label' => 'Продолжительность, дней',
                    'attribute' => 'duration',
                ],
                [
                    'label' => 'Статистика',
                    'format' => 'html',
                    'value' => function($data) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-user"></span>',
                            ['cabinet/statistics/admin-user', 'program_id' => $data->id],
                            [
                                'title' => 'Индивидуальная статистика',
                                'aria-label' => 'Индивидуальная статистика',
                            ]
                        ) . ' ' . Html::a(
                            '<span class="fa fa-users"></span>',
                            ['cabinet/statistics/admin-team', 'program_id' => $data->id],
                            [
                                'title' => 'Командная статистика',
                                'aria-label' => 'Командная статистика',
                            ]
                        );
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            if($model->start_date > time()) {
                                $options = [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'aria-label' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                            }
                            return '';
                        },
                        'update' => function ($url, $model, $key) {
                            if($model->start_date > time()) {
                                $options = [
                                    'title' => Yii::t('yii', 'Update'),
                                    'aria-label' => Yii::t('yii', 'Update'),
                                ];
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                            }
                            return '';
                        },
                    ]
                ],
            ],
        ]);
        ?>
    </div>
</article>