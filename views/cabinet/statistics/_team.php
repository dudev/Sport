<?php
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $dataProvider \yii\data\DataProviderInterface */
?>
<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'layout' => "{items}\n{pager}",
	'columns' => [
		//'id',
		[
			'class' => 'yii\grid\DataColumn',
			'attribute' => 'name',
			'label' => 'Название',
			'format' => 'html',
			'value' => function ($data) {
				return Html::a($data['name'], ['cabinet/team/team', 'id' => $data['id']]);
			},
		],
		[
			'class' => 'yii\grid\DataColumn',
			'label' => 'Упражнения',
			'format' => 'raw',
			'value' => function ($data) {
				$data = empty($data['activities']) ? [] : $data['activities'];
				$ret = '';
				foreach ($data as $v) {
					$ret .= Html::tag('p', $v['name'] . ': ' . round($v['value']));
				}
				return $ret ?: null;
			},
		],
		[
			'class' => 'yii\grid\DataColumn',
			'label' => 'Баллов',
			'attribute' => 'sum',
			'format' => 'integer',
		],
		[
			'class' => 'yii\grid\DataColumn',
			'label' => 'Среднее количество баллов',
			'format' => 'integer',
			'value' => function ($data) {
				if (empty($data['avg'])) {
					return null;
				} else {
					return array_sum($data['avg']) / count($data['avg']);
				}
			},
		],
	],
]);
?>