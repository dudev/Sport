<?php
use app\models\User;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\models\search\TeamSearch $model */
/** @var $this yii\web\View */
/** @var $dataProvider \yii\data\DataProviderInterface */
/** @var \app\models\forms\ImForm $im */
$this->title = 'Пользователи';

?>

<?php
$this->registerJs(<<<JS
$('#im-modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var modal = $(this)
  
  modal.find('#im-form').attr('action', button.attr('data-href'))
  modal.find('#im-modal-label').html(button.attr('data-label'))
})
JS
);
?>

<div class="modal fade" id="im-modal" tabindex="-1" role="dialog" aria-labelledby="im-modal-label">
    <div class="modal-dialog" role="document">
        <?php $form = ActiveForm::begin([
            'id' => 'im-form',
            'fieldConfig' => [
                'template' => '<div class="form-row">{input}{error}</div>',
            ],
            'enableClientValidation' => true,
        ]) ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="im-modal-label">Отправить сообщение</h3>
            </div>
            <div class="modal-body">
                <?= $form->field($im, 'message')->textarea(['placeholder' => $im->getAttributeLabel('message')]) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

<article class="container">
    <div class="page">
        <h1>Пользователи</h1>
        <?php if ($code): ?>
            <div class="alert alert-success" role="alert"><?= User::getMessage($code) ?></div>
        <?php endif; ?>
        <div class="form-group">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#im-modal"
                    data-href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/message']) ?>"
                    data-label="Отправить рассылку">
                Отправить сообщение всем пользователям
            </button>
        </div>

        <?php
        $this->registerJs(<<<JS
        $("#usersearch-city").click(function() {
            $("#usersearch-without_city").attr("checked", false);
        });
        $("#usersearch-without_city").change(function() {
        var city = $("#usersearch-city");
            if($(this).prop("checked")) {
                city.attr("data-value", city.val());
                city.val("");
            } else {
                city.val(city.attr("data-value"));
            }
        });
JS
);
        ?>

        <div class="panel panel-default">
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'search-form',
                    'fieldConfig' => [
                        'template' => '{input}',
                    ],
                    'enableClientValidation' => true,
                    'options' => [
                        'class' => 'form-inline',
                    ]
                ]); ?>
                <?= $form->field($model, 'nick')->textInput([
                    'placeholder' => $model->getAttributeLabel('nick')
                ]) ?>
                <?= $form->field($model, 'sex')->dropDownList(['Женский', 'Мужской'], ['prompt' => 'Пол']) ?>
                <?= $form->field($model, 'city')->textInput([
                    'placeholder' => $model->getAttributeLabel('city')
                ]) ?>
                <?= $form->field($model, 'without_city', [
                    'template' => '{label}{input}'
                ])->checkbox() ?>
                    <button type="submit" class="btn btn-success">Искать</button>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'label' => 'Имя',
                    'attribute' => 'nick',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::a($data['nick'], ['cabinet/user/index', 'id' => $data['id']]);
                    },
                ],
                [
                    'label' => 'Пол',
                    'attribute' => 'sex',
                    'value' => function ($data) {
                        $variants = ['Женский', 'Мужской'];
                        return $variants[ $data->sex ];
                    },
                ],
                [
                    'label' => 'Город',
                    'attribute' => 'city',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{edit} {message}',
                    'buttons' => [
                        'edit' => function ($url, $model, $key) {
                            $options = [
                                'title' => Yii::t('yii', 'Изменить данные пользователя'),
                                'aria-label' => Yii::t('yii', 'Изменить данные пользователя'),
                            ];
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                        },
                        'message' => function ($url, $model, $key) {
                            if ($model['id'] != Yii::$app->user->id) {
                                $options = [
                                    'title' => 'Отправить сообщение',
                                    'aria-label' => 'Отправить сообщение',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#im-modal',
                                    'data-href' => $url,
                                    'data-label' => 'Отправить сообщение пользователю',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-envelope"></span>', '#', $options);
                            }
                            return '';
                        },
                    ],
                ],
            ],
        ]);
        ?>
    </div>
</article>