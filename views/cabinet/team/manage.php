<?php
use app\controllers\cabinet\TeamController;
use app\models\Team;
use general\ext\DateHelper;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var integer $code */
/** @var Team $team */
/** @var \app\models\forms\TeamForm $team_form */
/** @var \app\models\forms\ImForm $im */
/** @var $this yii\web\View */
/** @var $dataProvider \yii\data\DataProviderInterface */
$this->title = 'Мои команды';

$this->registerJs(<<<JS
$('#new-member-modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var modal = $(this)
  
  modal.find('#new-member-form').attr('action', button.attr('data-href'))
  modal.find('#new-member-modal-label').html(button.attr('data-label'))
})
JS
);
?>

<div class="modal fade" id="new-member-modal" tabindex="-1" role="dialog" aria-labelledby="new-member-modal-label">
    <div class="modal-dialog" role="document">
        <?php $form = ActiveForm::begin([
            'id' => 'new-member-form',
            'fieldConfig' => [
                'template' => '<div class="form-row">{label}{input}{error}</div>',
            ],
            'enableClientValidation' => true,
        ]) ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="new-member-modal-label">Новый участник</h3>
            </div>
            <div class="modal-body">
                <?= $form->field($team_form, 'first_name0')->textInput() ?>
                <?= $form->field($team_form, 'last_name0')->textInput() ?>
                <?= $form->field($team_form, 'sex0')->dropDownList(['женский', 'мужской']) ?>
                <?= $form->field($team_form, 'email0')->textInput() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
$this->registerJs(<<<JS
$('#im-modal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var modal = $(this)
  
  modal.find('#im-form').attr('action', button.attr('data-href'))
  modal.find('#im-modal-label').html(button.attr('data-label'))
})
JS
);
?>

<div class="modal fade" id="im-modal" tabindex="-1" role="dialog" aria-labelledby="im-modal-label">
    <div class="modal-dialog" role="document">
        <?php $form = ActiveForm::begin([
            'id' => 'im-form',
            'fieldConfig' => [
                'template' => '<div class="form-row">{input}{error}</div>',
            ],
            'enableClientValidation' => true,
        ]) ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="im-modal-label">Отправить сообщение</h3>
            </div>
            <div class="modal-body">
                <?= $form->field($im, 'message')->textarea(['placeholder' => $im->getAttributeLabel('message')]) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

<article class="container">
    <div class="page">
        <h1><?= $team->name ?>
            <small>
                (<?= $team->program->name ?> c
                <?= date('j', $team->program->start_date) ?>
                <?= DateHelper::getMonthMin(date('n', $team->program->start_date)) ?>)
            </small>
        </h1>
        <?php if ($code): ?>
            <div class="alert alert-success" role="alert"><?= Team::getMessage($code) ?></div>
        <?php endif; ?>
        <?php if ($team->getUserTeams()->count() < TeamController::SIZE_OF_TEAM): ?>
            <div class="alert alert-danger" role="alert">
                В команде не хватает участников.
                <a href="#" data-toggle="modal" data-target="#new-member-modal"
                   data-href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/insert-user', 'id' => $team->id]) ?>"
                   data-label="Новый участник">Добавить участника?</a>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#im-modal"
                    data-href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/message', 'id' => $team->id]) ?>"
                    data-label="Отправить сообщение команде">
                Отправить сообщение всей команде
            </button>
            <?php if(Yii::$app->user->id == $team->captain): ?>
                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/pay', 'team_id' => $team->id]) ?>" role="button" class="btn btn-primary">
                    Оплатить участие
                </a>
            <?php endif; ?>
        </div>
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'label' => 'Имя',
                    'attribute' => 'user.nick',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::a($data['user']['nick'], ['cabinet/user/index', 'id' => $data['user_id']]);
                    },
                ],
                [
                    'label' => 'Подтверждено',
                    'format' => 'html',
                    'value' => function ($data) {
                        /** @var \app\models\UserTeam $data */
                        return $data->user_accept ? '<span class="glyphicon glyphicon-ok"></span>' : '';
                    },
                ],
                [
                    'label' => 'Оплачено',
                    'format' => 'html',
                    'value' => function ($data) {
                        /** @var \app\models\UserTeam $data */
                        return $data->is_paid ? '<span class="glyphicon glyphicon-ok"></span>' : '';
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete} {repeat} {replace-user} {replace-captain} {message}',
                    'urlCreator' => function ($action, $model, $key, $index) use ($team) {
                        $params['id'] = $team->id;
                        $params['user_id'] = $model->user_id;
                        $params[0] = 'cabinet/team/' . $action;

                        return Url::toRoute($params);
                    },
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            if ($model['user_id'] != Yii::$app->user->id) {
                                $options = [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'aria-label' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                            }
                            return '';
                        },
                        'repeat' => function ($url, $model, $key) {
                            if (!$model['user_accept']) {
                                $options = [
                                    'title' => 'Отправить приглашение повторно',
                                    'aria-label' => 'Отправить приглашение повторно',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-repeat"></span>', $url, $options);
                            }
                            return '';
                        },
                        'replace-user' => function ($url, $model, $key) {
                            if ($model['user_id'] != Yii::$app->user->id) {
                                $options = [
                                    'title' => 'Заменить участника',
                                    'aria-label' => 'Заменить участника',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#new-member-modal',
                                    'data-href' => $url,
                                    'data-label' => 'Замена участника',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-refresh"></span>', '#', $options);
                            }
                            return '';
                        },
                        'replace-captain' => function ($url, $model, $key) use ($team) {
                            if ($model['user_id'] != Yii::$app->user->id && $model['user_id'] != $team->captain) {
                                $options = [
                                    'title' => 'Назначить капитаном',
                                    'aria-label' => 'Назначить капитаном',
                                    'data-confirm' => 'Подтвердите изменение капитана.',
                                    'data-method' => 'post',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-check"></span>', $url, $options);
                            }
                            return '';
                        },
                        'message' => function ($url, $model, $key) {
                            if ($model['user_id'] != Yii::$app->user->id) {
                                $options = [
                                    'title' => 'Отправить сообщение',
                                    'aria-label' => 'Отправить сообщение',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#im-modal',
                                    'data-href' => $url,
                                    'data-label' => 'Отправить сообщение пользователю',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-envelope"></span>', '#', $options);
                            }
                            return '';
                        },
                    ],
                ],
            ],
        ]);
        ?>
    </div>
</article>