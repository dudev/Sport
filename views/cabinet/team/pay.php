<?php
/** @var $this yii\web\View */
/** @var \app\models\Team $team */
use general\ext\DateHelper;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$users = [];
foreach ($team->users as $user) {
    /** @var \app\models\User $user */
    $users[$user->id] = $user->nick;
}

$this->registerJs(<<<JS
var team_id = $team->id;
var price = 1000;
$('.users').change(function() {
    var users = new Array;
    var checked_users = $('.users:checked');
    checked_users.each(function() {
        users.push($(this).val());
    });
    $('#calcSum').text(checked_users.length * 1000);
    $('[name=sum]').val(checked_users.length * 1000);
    $('[name=label]').val(team_id + ':' + users.join(','));
});
JS
);

?>
<article class="container">
    <div class="page">
        <h1>Оплата участия</h1>
        <h2><?= $team->name ?>
            <small>
                (<?= $team->program->name ?> c
                <?= date('j', $team->program->start_date) ?>
                <?= DateHelper::getMonthMin(date('n', $team->program->start_date)) ?>)
            </small>
        </h2>
        <div class="form">
            <?php
            $form = ActiveForm::begin([
                'id' => 'post-form',
                'fieldConfig' => [
                    'template' => '<div class="form-row">{label}{input}{error}</div>',
                ],
                'action' => 'https://money.yandex.ru/quickpay/confirm.xml'
            ]);
            ?>
            <?= Html::hiddenInput('receiver', '410012123098328') ?>
            <?= Html::hiddenInput('quickpay-form', 'shop') ?>
            <?= Html::hiddenInput('targets', 'Оплата на сайте crosschallenge.ru') ?>
            <?= Html::hiddenInput('paymentType', 'AC') ?>
            <?= Html::hiddenInput('sum', '1000') ?>
            <?= Html::hiddenInput('label', $team->id . ':') ?>
            <?= Html::hiddenInput('successURL', 'http://crosschallenge.ru') ?>
            <?= Html::checkboxList(
                'users',
                [Yii::$app->user->id],
                $users,
                [
                    'item' => function ($index, $label, $name, $checked, $value) {
                        $line = '<div class="input-group">';
                        $line .= Html::checkbox($name, $checked, [
                            'value' => $value,
                            'label' => $label,
                            'class' => 'users',
                        ]);
                        $line .= '</div>';
                        return $line;
                    }
                ]
            ) ?>
            <p>К оплате <span id="calcSum"><?= 1000 ?></span> руб.</p>
            <div class="form-group">
                <?= Html::submitButton('Оплатить', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</article>
