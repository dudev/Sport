<?php
use app\models\Program;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\TeamForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $number integer */

$this->title = 'Создать команду';
?>
<article class="container">
	<div class="page">
		<h1><?= $this->title ?></h1>

		<div class="form">
			<?php $form = ActiveForm::begin([
				'id' => 'post-form',
				'fieldConfig' => [
					'template' => '<div class="form-row">{label}{input}{error}</div>',
				],
				'enableClientValidation' => true,
			]); ?>

			<?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

			<?= $form->field($model, 'program_id')->dropDownList(Program::getNextProgram()) ?>

			<h2>Состав команды</h2>

			<div class="row">
				<?php for($i = 0; $i < $number; $i++): ?>
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Участник №<?= ($i + 1) ?></h3>
							</div>
							<div class="panel-body">
								<?= $form->field($model, 'first_name' . $i)->textInput() ?>
								<?= $form->field($model, 'last_name' . $i)->textInput() ?>
								<?= $form->field($model, 'sex' . $i)->dropDownList(['женский', 'мужской']) ?>
								<?= $form->field($model, 'email' . $i)->textInput() ?>
							</div>
						</div>
					</div>
					<?php if(($i + 1) % 2 == 0 || ($i + 1) % 3 == 0): ?>
						<div class="clearfix<?= (($i + 1) % 2 == 0 ? ' visible-sm' : '')
							. (($i + 1) % 3 == 0 ? ' visible-md' : '')
							. (($i + 1) % 4 == 0 ? ' visible-lg' : '')
						?>"></div>
					<?php endif; ?>
				<?php endfor; ?>
			</div>


			<div class="form-group">
				<?= Html::submitButton('Создать', ['class' => 'btn btn-success btn-block btn-lg']) ?>
			</div>

			<?php ActiveForm::end(); ?>

		</div>
	</div>
</article>