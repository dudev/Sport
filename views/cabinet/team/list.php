<?php
use app\models\Program;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\models\search\TeamSearch $model */
/** @var $this yii\web\View */
/** @var $dataProvider \yii\data\DataProviderInterface */
$this->title = 'Команды';

?>

<article class="container">
    <div class="page">
        <h1>Команды</h1>
        <div class="form-group">
            <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/create']) ?>" role="button" class="btn btn-primary">
                Создать команду
            </a>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'search-form',
                    'fieldConfig' => [
                        'template' => '{input}',
                    ],
                    'enableClientValidation' => true,
                    'options' => [
                        'class' => 'form-inline',
                    ]
                ]); ?>
                <?= $form->field($model, 'name')->textInput([
                    'placeholder' => $model->getAttributeLabel('name')
                ]) ?>
                <?= $form->field($model, 'is_completed')->dropDownList(['Не сформирована', 'Сформирована'], ['prompt' => 'Команда сформирована?']) ?>
                <?= $form->field($model, 'program_id')->dropDownList(Program::getPrograms(), ['prompt' => 'Программа']) ?>
                    <button type="submit" class="btn btn-success">Искать</button>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'label' => 'Программа',
                    'attribute' => 'program.name',
                ],
                [
                    'label' => 'Команда',
                    'attribute' => 'name',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::a($data->name, ['cabinet/team/team', 'id' => $data->id]);
                    },
                ],
                [
                    'label' => 'Сформирована',
                    'format' => 'html',
                    'value' => function ($data) {
                        /** @var \app\models\Team $data */
                        return $data->is_completed ? '<span class="glyphicon glyphicon-ok"></span>' : '';
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{manage}',
                    'buttons' => [
                        'manage' => function ($url, $model, $key) {
                            $options = [
                                'title' => Yii::t('yii', 'Управление командой'),
                                'aria-label' => Yii::t('yii', 'Управление командой'),
                            ];
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                        },
                    ],
                ],
            ],
        ]);
        ?>
    </div>
</article>