<?php
use app\models\Team;
use general\ext\DateHelper;

/** @var integer $code */
/** @var array $teams */
/** @var array $invites */
/** @var $this yii\web\View */
$this->title = 'Мои команды';
?>
<article class="container">
    <div class="page">
        <h1><?= $this->title ?></h1>
        <?php if($code): ?>
            <div class="alert alert-success" role="alert"><?= Team::getMessage($code) ?></div>
        <?php endif; ?>
        <?php if ($teams) {
            /** @var Team $team */
            foreach ($teams as $team) { ?>
                <h2><?= $team->name ?>
                    <small>
                        <?php if($team->captain == Yii::$app->user->id): ?>
                            <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/manage', 'id' => $team->id]) ?>" class="glyphicon glyphicon-pencil" style="color: inherit;"></a>
                        <?php endif; ?>
                        (<?= $team->program->name ?> c
                        <?= date('j', $team->program->start_date) ?>
                        <?= DateHelper::getMonthMin(date('n', $team->program->start_date)) ?>)
                    </small>
                </h2>
                <ul>
                    <?php
                    /** @var app\models\User $user */
                    foreach ($team->users as $user) { ?>
                        <li>
                            <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/profile', 'id' => $user->id]) ?>"><?= $user->nick ?></a>
                            <?= ($user->id == $team->captain ? ' <small>(капитан)</small>' : '') ?>
                        </li>
                    <?php } ?>
                </ul>
            <?php }
        } else { ?>
            <p>
                Вы не состоите ни в одной команде.
            </p>
            <p>
                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/create']) ?>" class="btn btn-success">Создать
                    команду?</a>
            </p>
        <?php }

        if ($invites): ?>
            <h2>Приглашения</h2>

            <?php /** @var Team $invite */
            foreach ($invites as $invite): ?>
                <p>
                    <?= $invite->captainUser->nick ?> приглашает в программу <?= $invite->program->name ?>

                    (с <?= date('j', $invite->program->start_date) ?>
                    <?= DateHelper::getMonthMin(date('n', $invite->program->start_date)) ?>

                    до <?= date('j', strtotime('+100 day', $invite->program->start_date)) ?>
                    <?= DateHelper::getMonthMin(date('n', strtotime('+100 day', $invite->program->start_date))) ?>).
                </p>
                <p>
                    <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/user-accept', 'team_id' => $invite->id]) ?>" class="btn btn-success">Принять</a>
                    <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/user-reject', 'team_id' => $invite->id]) ?>" class="btn btn-warning">Отклонить</a>
                </p>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</article>