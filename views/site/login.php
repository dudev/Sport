<?php
use general\widgets\api\LoginForm;

$this->title = 'Аутентификация';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
	#login {
		border: 0 none;
		display: block;
		height: 330px;
		margin: 0 auto;
		width: 340px;
	}
</style>
<?= LoginForm::widget([
	'service' => Yii::$app->id,
	'view' => 'login',
	'retUrl' => '//' . Yii::$app->params['api']['loginUrls'][ Yii::$app->id ],
	'class' => 'login',
	'id' => 'login',
]);
?>