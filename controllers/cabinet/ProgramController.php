<?php

namespace app\controllers\cabinet;

use app\extensions\CabinetController;
use app\models\forms\ProgramForm;
use app\models\Program;
use app\models\search\ProgramSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class ProgramController extends CabinetController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->is_admin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($code = 0)
    {
        $model = new ProgramSearch();

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $model->search(Yii::$app->request->post()),
            'code' => $code,
        ]);
    }

    public function actionCreate()
    {
        $model = new ProgramForm();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['cabinet/program', 'success' => 1]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = ProgramForm::findOne($id);

        if(!$model) {
            throw new NotFoundHttpException;
        }

        if($model->start_date < time()) {
            throw new ForbiddenHttpException;
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['cabinet/program', 'success' => 1]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = Program::findOne($id);

        if(!$model) {
            throw new NotFoundHttpException;
        }

        if ($model->delete()) {
            $this->redirect(['cabinet/program', 'success' => 1]);
        } else {
            $this->redirect(['cabinet/program', 'success' => 1]);
        }
    }
}