<?php

namespace app\controllers\cabinet;

use app\extensions\CabinetController;
use app\models\Team;
use app\models\User;
use app\models\UserActivityVariant;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;

class SiteController extends CabinetController {
	public $defaultAction = 'dashboard';
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['dashboard'],
				'rules' => [
					[
						'actions' => ['dashboard'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionDashboard() {
        $team = Team::find()
            ->where(['user_team.user_id' => Yii::$app->user->id])
            ->andWhere(['>', 'program.start_date', new Expression('NOW() - INTERVAL program.duration DAY')])
            ->innerJoinWith('userTeams', false)
            ->innerJoinWith('program', false)
            ->orderBy(['program.start_date' => SORT_ASC])
            ->limit(1)
            ->one();

        $results = UserActivityVariant::find()
            ->select('SUM(user_activity_variant.value * activity_variant.coefficient)')
            ->innerJoinWith('activityVariant', false)
            ->where(['user_activity_variant.user_id' => Yii::$app->user->id])
            ->groupBy('user_activity_variant.date')
            ->asArray()
            ->column();
        //среднее
        $average = $results ? array_sum($results) / count($results) : 0;
        //рекорд
        $maximum = $results ? max($results) : 0;

	    return $this->render('dashboard', [
            'team' => $team,
            'average' => $average,
            'maximum' => $maximum,
			'is_avatar' => is_file(Yii::getAlias('@webroot') . User::PATH_TO_AVATARS . Yii::$app->user->id . '.jpg'),
        ]);
    }
}