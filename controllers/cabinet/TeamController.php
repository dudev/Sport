<?php

namespace app\controllers\cabinet;

use app\extensions\CabinetController;
use app\models\forms\ImForm;
use app\models\forms\TeamForm;
use app\models\search\TeamSearch;
use app\models\Team;
use app\models\UserActivityVariant;
use app\models\UserTeam;
use general\ext\api\passport\PassportApi;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class TeamController extends CabinetController
{
    const SIZE_OF_TEAM = 5;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'create',
                            'user-accept',
                            'user-reject',
                            'pay',
                            'team',
                            'manage',
                            'delete',
                            'repeat',
                            'replace-user',
                            'replace-captain',
                            'insert-user',
                            'message',
                            'list'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'manage',
                            'delete',
                            'repeat',
                            'replace-user',
                            'replace-captain',
                            'insert-user',
                            'message'
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if (!$team = $this->getTeam(Yii::$app->request->get('id'))) {
                                throw new NotFoundHttpException;
                            }

                            if (Yii::$app->user->identity->is_admin) {
                                return true;
                            }

                            if ($team->captain != Yii::$app->user->id) {
                                return false;
                            }

                            return true;
                        },
                    ],
                    [
                        'actions' => ['list'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->is_admin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($code = 0)
    {
        //информаци о текущем состоянии: состоит\не состоит в команде
        //если состоит: название, капитан и остальные члены команды
        //если не состоит: предложение создать команду
        $teams = Team::find()
            ->where([
                'user_team.user_id' => Yii::$app->user->id,
                'user_team.team_accept' => 1,
                'user_team.user_accept' => 1,
            ])->andWhere([
                '>', 'program.start_date', new Expression('NOW() - INTERVAL program.duration DAY')
            ])
            ->innerJoinWith('program')//используется в представлении
            ->innerJoinWith('users')//используется в представлении
            ->all();
        //приглашения
        $invites = Team::find()
            ->where([
                'user_team.user_id' => Yii::$app->user->id,
                'user_team.team_accept' => 1,
                'user_team.user_accept' => 0,
            ])->andWhere([
                '>', 'program.start_date', new Expression('NOW()')
            ])
            ->innerJoinWith('program')//используется в представлении
            ->innerJoinWith('userTeams', false)
            ->with('captainUser')//используется в представлении
            ->all();
        return $this->render('index', [
            'teams' => $teams,
            'invites' => $invites,
            'code' => $code,
        ]);
    }

    public function actionCreate()
    {
        //создание команды
        $model = new TeamForm(['numberOfMembers' => self::SIZE_OF_TEAM]);
        if ($model->load(Yii::$app->request->post())) {
            if ($team = $model->create()) {
                /** @var Team $team */
                return $this->redirect(['cabinet/team/pay', 'team_id' => $team->id]);
            } else {
                //var_dump($model->errors);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'number' => self::SIZE_OF_TEAM - 1
        ]);
    }

    public function actionUserAccept($team_id)
    {
        /** @var UserTeam $model */
        $model = UserTeam::find()
            ->where([
                'team_id' => $team_id,
                'user_id' => Yii::$app->user->id
            ])
            ->limit(1)
            ->one();
        $model->user_accept = 1;
        $model->save();
        return $this->redirect(['cabinet/team/index', [
            'code' => Team::USER_ACCEPT,
        ]]);
    }

    public function actionUserReject($team_id)
    {
        /** @var UserTeam $model */
        $model = UserTeam::find()
            ->where([
                'team_id' => $team_id,
                'user_id' => Yii::$app->user->id
            ])
            ->limit(1)
            ->one();
        $model->delete();

        try {
            /** @var \yii\swiftmailer\Mailer $mailer */
            $mailer = Yii::$app->mailer;

            /** @var Team $team */
            $team = Team::findOne($team_id);
            $passport = PassportApi::passportSearch(['user_id' => $team->captain]);

            if ($passport['result'] == 'success') {
                $email_to = $passport['passports'][0]['email'];
                $mailer->compose(['text' => 'team/invite_reject_text'], ['user' => Yii::$app->user->identity, 'team' => $team])
                    ->setFrom(Yii::$app->params['email_from'])
                    ->setTo($email_to)
                    ->setSubject('Приглашение отклонено')
                    ->send();
            }
        } catch (\Exception $e) {
        }

        return $this->redirect(['cabinet/team/index', [
            'code' => Team::USER_REJECT,
        ]]);
    }

    public function actionPay($team_id)
    {
        $team = Team::find()
            ->where([
                'team.id' => $team_id,
                'team.captain' => Yii::$app->user->id,
            ])/*->andWhere([
                '>', 'program.start_date', new Expression('NOW()')
            ])*/
            //@todo возможно, стоит обратно включить проверку даты
            ->innerJoinWith('program')//используется в представлении
            ->with('users')//используется в представлении
            ->limit(1)
            ->one();

        if (!$team) {
            return $this->redirect('index');
        }

        return $this->render('pay', [
            'team' => $team,
        ]);
    }

    public function actionTeam($id)
    {
        $team = Team::find()
            ->where(['team.id' => $id])
            ->innerJoinWith('program')
            ->limit(1)
            ->one();

        $activities = UserActivityVariant::find()
            ->select([
                new Expression('(user_activity_variant.team_value * activity_variant.coefficient) as sum'),
                'activity_variant.activity_id activity_id',
                'activity.name name',
                'user_activity_variant.date date',
            ])
            ->where(['user_team.team_id' => $id])
            ->andWhere(['>=', 'user_activity_variant.date', new Expression('program.start_date')])
            ->andWhere(['<', 'user_activity_variant.date', new Expression('program.start_date + INTERVAL program.duration DAY')])
            ->innerJoinWith('user.teams.program', false)
            ->innerJoinWith('activityVariant.activity', false)
            ->orderBy(['activity_variant.activity_id' => SORT_ASC, 'user_activity_variant.date' => SORT_ASC])
            ->asArray()
            ->all();

        return $this->render('team', [
            'team' => $team,
            'activities' => $activities,
        ]);
    }

    public function actionManage($id, $code = 0)
    {
        $team = $this->getTeam($id);

        $dataProvider = new ActiveDataProvider([
            'query' => $team->getUserTeams()->with('user'),
        ]);

        return $this->render('manage', [
            'team' => $team,
            'dataProvider' => $dataProvider,
            'code' => $code,
            'team_form' => (new TeamForm()),
            'im' => (new ImForm()),
        ]);
    }

    public function actionDelete($id, $user_id)
    {
        //@todo убрать флаг в команде
        if ($user_id != Yii::$app->user->id) {
            $user_team = UserTeam::findOne([
                'team_id' => $id,
                'user_id' => $user_id
            ]);
            if ($user_team) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $team = $user_team->team;
                    $team->is_completed = 0;
                    $team->save();

                    $user_team->delete();

                    $transaction->commit();

                    try {
                        /** @var \yii\swiftmailer\Mailer $mailer */
                        $mailer = Yii::$app->mailer;

                        $passport = PassportApi::passportSearch(['user_id' => $user_id]);
                        if($passport['result'] == 'success') {
                            $email_to = $passport['passports'][0]['email'];
                            $mailer->compose(['text' => 'team/delete_from_team_text'], ['user' => Yii::$app->user->identity, 'team' => $team])
                                ->setFrom(Yii::$app->params['email_from'])
                                ->setTo($email_to)
                                ->setSubject('Вас исключили из команды')
                                ->send();
                        }
                    } catch (\Exception $e) {
                    }

                    $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_DELETE_MEMBER_SUCCESS]);
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_DELETE_MEMBER_ERROR]);
                }
            } else {
                throw new NotFoundHttpException;
            }
        } else {
            throw new BadRequestHttpException;
        }
    }

    public function actionRepeat($id, $user_id)
    {
        $user_team = UserTeam::findOne([
            'team_id' => $id,
            'user_id' => $user_id
        ]);
        if ($user_team) {
            if ($user_team->user_accept) {
                throw new BadRequestHttpException;
            } else {
                $passport = PassportApi::passportSearch(['user_id' => $user_id]);
                if ($passport['result'] == 'success') {
                    TeamForm::sendInvite($passport['passports'][0]['email'], $this->getTeam($id));
                    $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_REPEAT_SUCCESS]);
                } else {
                    $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_REPEAT_ERROR]);
                }
            }
        } else {
            throw new NotFoundHttpException;
        }
    }

    /**
     * @param integer $id
     * @param integer $user_id
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionReplaceUser($id, $user_id)
    {
        if ($user_id != Yii::$app->user->id) {
            $user_team = UserTeam::findOne([
                'team_id' => $id,
                'user_id' => $user_id
            ]);
            if ($user_team) {
                $team_form = new TeamForm();
                if ($team_form->replaceUser($user_team, Yii::$app->request->post(), $this->getTeam($id))) {
                    try {
                        /** @var \yii\swiftmailer\Mailer $mailer */
                        $mailer = Yii::$app->mailer;

                        $passport = PassportApi::passportSearch(['user_id' => $user_id]);
                        if($passport['result'] == 'success') {
                            $email_to = $passport['passports'][0]['email'];
                            $mailer->compose(['text' => 'team/delete_from_team_text'], ['user' => Yii::$app->user->identity, 'team' => $user_team->team])
                                ->setFrom(Yii::$app->params['email_from'])
                                ->setTo($email_to)
                                ->setSubject('Вас исключили из команды')
                                ->send();
                        }
                    } catch (\Exception $e) {
                    }
                    $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_REPLACE_USER_SUCCESS]);
                } else {
                    $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_REPLACE_USER_ERROR]);
                }
            } else {
                throw new NotFoundHttpException;
            }
        } else {
            throw new BadRequestHttpException;
        }
    }

    public function actionReplaceCaptain($id, $user_id)
    {
        if ($user_id != Yii::$app->user->id) {
            $user_team = UserTeam::findOne([
                'team_id' => $id,
                'user_id' => $user_id
            ]);
            if ($user_team) {
                $team = $this->getTeam($id);
                if($team->captain != $user_id) {
                    $old_captain = $team->captain;
                    
                    $team->captain = $user_id;
                    if ($team->save()) {
                        if($old_captain == Yii::$app->user->id) {
                            $this->redirect(['cabinet/team']);
                        } else {
                            //@todo уведомление на почту
                            $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_REPLACE_CAPTAIN_SUCCESS]);
                        }
                    } else {
                        $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_REPLACE_CAPTAIN_ERROR]);
                    }
                } else {
                    throw new BadRequestHttpException;
                }
            } else {
                throw new NotFoundHttpException;
            }
        } else {
            throw new BadRequestHttpException;
        }
    }

    /**
     * @param integer $id
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionInsertUser($id)
    {
        $team = $this->getTeam($id);

        if ($team->getUserTeams()->count() >= self::SIZE_OF_TEAM) {
            throw new BadRequestHttpException;
        }

        $team_form = new TeamForm();
        if ($team_form->insertUser(Yii::$app->request->post(), $this->getTeam($id))) {
            $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_INSERT_USER_SUCCESS]);
        } else {
            $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_INSERT_USER_ERROR]);
        }
    }

    public function actionMessage($id, $user_id = 0)
    {
        if ($user_id == Yii::$app->user->id) {
            throw new BadRequestHttpException;
        }

        if (!Yii::$app->request->isPost) {
            throw new \HttpRequestMethodException;
        }

        if ($user_id != 0) {
            $user_team = UserTeam::findOne([
                'team_id' => $id,
                'user_id' => $user_id
            ]);
            if ($user_team) {
                $user_ids = $user_id;
            } else {
                throw new NotFoundHttpException;
            }
        } else {
            $user_ids = UserTeam::find()
                ->select('user_id')
                ->where(['team_id' => $id])
                ->asArray()
                ->column();

            if (!$user_ids) {
                throw new NotFoundHttpException;
            }
        }
        $im = new ImForm();
        $im->load(Yii::$app->request->post());

        if ($im->send($user_ids)) {
            $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_SEND_MESSAGE_SUCCESS]);
        } else {
            $this->redirect(['cabinet/team/manage', 'id' => $id, 'code' => Team::MANAGE_SEND_MESSAGE_ERROR]);
        }
    }

    public function actionList()
    {
        $model = new TeamSearch();

        return $this->render('list', [
            'model' => $model,
            'dataProvider' => $model->search(Yii::$app->request->post())
        ]);
    }

    private $_teams = [];

    /**
     * @param $id
     * @return Team|null
     */
    private function getTeam($id)
    {
        if (!empty($this->_teams[$id])) {
            return $this->_teams[$id];
        }

        if ($id > 0) {
            return $this->_teams[$id] = Team::findOne($id);
        }

        return null;
    }
}