<?php

namespace app\controllers\cabinet;

use app\extensions\CabinetController;
use app\models\forms\ImForm;
use app\models\forms\UserForm;
use app\models\search\UserSearch;
use app\models\Team;
use app\models\User;
use app\models\UserActivityVariant;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class UserController extends CabinetController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'list', 'edit', 'message'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['list', 'edit', 'message'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->is_admin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($id)
    {
        $user = User::findOne($id);

        if ($user && $user->privacy == User::PRIVACY_CLOSE) {
            throw new NotFoundHttpException;
        }

        $team = Team::find()
            ->where(['user_team.user_id' => $id])
            ->andWhere(['>', 'program.start_date', new Expression('NOW() - INTERVAL program.duration DAY')])
            ->innerJoinWith('userTeams', false)
            ->innerJoinWith('program', false)
            ->orderBy(['program.start_date' => SORT_ASC])
            ->limit(1)
            ->one();

        $results = UserActivityVariant::find()
            ->select('SUM(user_activity_variant.value * activity_variant.coefficient)')
            ->innerJoinWith('activityVariant', false)
            ->where(['user_activity_variant.user_id' => $id])
            ->groupBy('user_activity_variant.date')
            ->asArray()
            ->column();
        //среднее
        $average = $results ? array_sum($results) / count($results) : 0;
        //рекорд
        $maximum = $results ? max($results) : 0;

        $activities = [];
        if ($user->privacy == User::PRIVACY_OPEN && $team) {
            $activities = UserActivityVariant::find()
                ->select([
                    '(user_activity_variant.value * activity_variant.coefficient) as sum',
                    'activity_variant.activity_id activity_id',
                    'activity.name name',
                    'user_activity_variant.date date',
                ])
                ->where([
                    'user_team.team_id' => $team->id,
                    'user_activity_variant.user_id' => $id,
                ])
                ->andWhere(['>=', 'user_activity_variant.date', new Expression('program.start_date')])
                ->andWhere(['<', 'user_activity_variant.date', new Expression('program.start_date + INTERVAL program.duration DAY')])
                ->innerJoinWith('user.teams.program', false)
                ->innerJoinWith('activityVariant.activity', false)
                ->orderBy(['activity_variant.activity_id' => SORT_ASC, 'user_activity_variant.date' => SORT_ASC])
                ->asArray()
                ->all();
        }

        return $this->render('index', [
            'model' => $user,
            'team' => $team,
            'average' => $average,
            'maximum' => $maximum,
            'activities' => $activities,
            'is_avatar' => is_file(Yii::getAlias('@webroot') . User::PATH_TO_AVATARS . Yii::$app->user->id . '.jpg'),
        ]);
    }

    public function actionUpdate($success = 0)
    {
        $user = UserForm::findOne();

        if (Yii::$app->request->isPost) {
            if ($user->load(Yii::$app->request->post()) && $user->save()) {
                $this->redirect(['cabinet/user/update', 'success' => 1]);
            }
        }

        return $this->render('update', [
            'model' => $user,
            'success' => $success,
        ]);
    }

    public function actionList($code = 0)
    {
        $model = new UserSearch();

        return $this->render('list', [
            'model' => $model,
            'dataProvider' => $model->search(Yii::$app->request->post()),
            'im' => (new ImForm()),
            'code' => $code,
        ]);
    }

    public function actionEdit($id)
    {
        $user = UserForm::findOne($id);

        if (Yii::$app->request->isPost && $user->load(Yii::$app->request->post()) && $user->save()) {
            $this->redirect(['cabinet/user/list', 'code' => User::EDIT_USER_SUCCESS]);
        }

        return $this->render('edit', [
            'model' => $user,
        ]);
    }

    public function actionMessage($id = 0)
    {
        if ($id == Yii::$app->user->id) {
            throw new BadRequestHttpException;
        }

        if (!Yii::$app->request->isPost) {
            throw new \HttpRequestMethodException;
        }

        if ($id == 0) {
            $user_ids = User::find()
                ->select('id')
                ->column();
        } else {
            if (User::findOne($id)) {
                throw new NotFoundHttpException;
            }
            $user_ids = $id;
        }
        $im = new ImForm();
        $im->load(Yii::$app->request->post());

        if ($im->send($user_ids)) {
            $this->redirect(['cabinet/user/list', 'code' => User::MANAGE_SEND_MESSAGE_SUCCESS]);
        } else {
            $this->redirect(['cabinet/user/list', 'code' => User::MANAGE_SEND_MESSAGE_ERROR]);
        }
    }
}