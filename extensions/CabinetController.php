<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions;

/**
 * Class CabinetController
 */
class CabinetController extends Controller {
	public $layout = 'cabinet';
} 