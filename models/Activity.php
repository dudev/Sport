<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $name
 * @property string $measure
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ActivityVariant[] $activityVariants
 */
class Activity extends \yii\db\ActiveRecord
{
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'measure', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['measure'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'measure' => 'Measure',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityVariants()
    {
        return $this->hasMany(ActivityVariant::className(), ['activity_id' => 'id']);
    }

	public static function getActivities() {
		$data = ActivityVariant::find()
			->where(['<=', 'program.start_date', new Expression('NOW()')])
			->andWhere(['>', 'program.start_date', new Expression('NOW() - INTERVAL program.duration DAY')])
			->andWhere([
				'user_team.user_id' => Yii::$app->user->id,
				'user_team.team_accept' => 1,
				'user_team.user_accept' => 1,
				//'user_team.is_paid' => 1,
				'activity_variant.sex' => (int)Yii::$app->user->identity->sex,
			])
			//@todo включить проверку оплаты
			->innerJoinWith('programActivityVariants.program.teams.userTeams', false)
			->with('activity') //используется в представлении
			->all();

		$ret = [];
		/** @var ActivityVariant $v */
		foreach ($data as $v) {
			$ret[ $v->id ] = $v->activity->name . ' (' . $v->activity->measure . ')';
		}

		return $ret;
	}
}
