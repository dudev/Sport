<?php
namespace app\models\forms;

use app\models\Program;
use app\models\Team;
use app\models\User;
use app\models\UserTeam;
use general\ext\api\auth\AuthApi;
use general\ext\api\passport\PassportApi;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use general\controllers\api\Controller as ApiController;

/**
 * Team form
 */
class TeamForm extends Model
{
    public $isExactNumberOfMembers = false;
    public $numberOfMembers = 5;

    public $name;
    public $program_id;
    //@todo сделать сеттеры и геттеры
    public $email0;
    public $email1;
    public $email2;
    public $email3;
    public $email4;
    public $first_name0;
    public $first_name1;
    public $first_name2;
    public $first_name3;
    public $first_name4;
    public $last_name0;
    public $last_name1;
    public $last_name2;
    public $last_name3;
    public $last_name4;
    public $sex0;
    public $sex1;
    public $sex2;
    public $sex3;
    public $sex4;

    public function rules()
    {
        $rules = [
            [['name', 'program_id'], 'required'],
            ['name', 'string', 'min' => 4, 'max' => 50],

            ['program_id', 'integer'],
            ['program_id', 'exist', 'targetClass' => Program::className(), 'targetAttribute' => 'id'],
            ['program_id', 'checkProgram'],
            ['email0', 'emailsCheck'],
        ];
        for ($i = 0; $i < $this->numberOfMembers - (int)(!$this->isExactNumberOfMembers); $i++) {
            $rules[] = [['email' . $i, 'first_name' . $i, 'last_name' . $i], 'required'];
            $rules[] = ['email' . $i, 'trim'];
            $rules[] = ['email' . $i, 'email'];
            $rules[] = [['first_name' . $i, 'last_name' . $i], 'string', 'min' => 2, 'max' => 50];
            $rules[] = ['sex' . $i, 'default', 'value' => '0'];
            $rules[] = ['sex' . $i, 'boolean'];
        }
        return $rules;
    }

    public function emailsCheck($attribute, $params)
    {
        $emails = [];
        for ($i = 0; $i < ($this->numberOfMembers - (int)(!$this->isExactNumberOfMembers)); $i++) {
            $emails[] = $this->{'email' . $i};
        }
        //Уникальны ли почты?
        $unique_emails = array_unique($emails, SORT_STRING);
        if (count($emails) != count($unique_emails)) {
            $duplicate_emails = [];
            foreach ($unique_emails as $val) {
                if (count(array_keys($emails, $val, true)) > 1) {
                    $duplicate_emails[] = $val;
                }
            }
            foreach ($duplicate_emails as $val) {
                foreach (array_keys($emails, $val, true) as $v) {
                    $this->addError('email' . $v, 'Адрес эл. почты повторяется.');
                }
            }
            return;
        }

        //Состоит ли в команде на эту программу?
        foreach ($emails as $email) {
            $passport = PassportApi::passportSearch([
                'email' => $email
            ]);
            if ($passport['result'] == 'success') {
                $user_id = $passport['passports'][0]['user_id'];
                $is_registered = UserTeam::find()
                    ->where(['user_team.user_id' => $user_id, 'team.program_id' => $this->program_id])
                    ->innerJoinWith('team', false)
                    ->exists();
                if ($is_registered) {
                    $this->addError('email' . array_search($email, $emails, true), 'Уже состоит в другой команде.');
                }
            }
        }
        //@todo возможно, стоит проверять пересечение программ, чтобы не получилось, что человек участвует одновременно в нескольких программах
    }

    public function checkProgram($attribute, $params)
    {
        //Не началась ли уже программа?
        $is_started = !Program::find()
            //@todo включить фильтр
            //->where(['>', 'start_date', new Expression('NOW()')])
            ->andWhere(['id' => $this->program_id])
            ->exists();
        if ($is_started) {
            $this->addError('program_id', 'Программа уже началась.');
        }
        if (!$this->isExactNumberOfMembers) {
            $is_registered = UserTeam::find()
                ->where(['user_team.user_id' => Yii::$app->user->id, 'team.program_id' => $this->program_id])
                ->innerJoinWith('team', false)
                ->exists();
            if ($is_registered) {
                $this->addError('program_id', 'Вы состоите в другой команде, участвующей в этой программе.');
            }
        }
    }

    /**
     * Создание команды
     * @return bool
     * @throws Exception
     */
    public function create()
    {
        if (!$this->isExactNumberOfMembers) {
            $users[] = Yii::$app->user->id;
        } else {
            $users = [];
        }
        if ($this->validate()) {
            for ($i = 0; $i < ($this->numberOfMembers - (int)(!$this->isExactNumberOfMembers)); $i++) {
                if($user_id = $this->signupUser($i)) {
                    $users[] = $user_id;
                }
            }
            if (!$this->isExactNumberOfMembers && ($key = array_search(Yii::$app->user->id, array_slice($users, 1))) !== false) {
                $this->addError('email' . $key, 'Вы пытаетесь добавить самого себя.');
            }
            if (!$this->hasErrors()) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $team = new Team();
                    $team->name = $this->name;
                    $team->program_id = $this->program_id;
                    $team->captain = $users[0];
                    if (!$team->save()) {
                        throw new Exception('Ошибка валидации');
                    }

                    foreach ($users as $user_id) {
                        $this->createUserTeam($user_id, $team);
                    }
                    $transaction->commit();

                    try {
                        for ($i = 0; $i < ($this->numberOfMembers - (int)(!$this->isExactNumberOfMembers)); $i++) {
                            self::sendInvite($this->{'email' . $i}, $team);
                        }
                    } catch (\Exception $e) {
                    }

                    return $team;
                } catch (Exception $e) {
                    $this->addError('name', 'Ошибка при работе с базой данных.');
                    $transaction->rollBack();
                    //throw $e;
                    return null;
                }
            }
        }
        return null;
    }

    public function createUserTeam($user_id, $team) {
        $user_team = new UserTeam();
        $user_team->team_id = $team->id;
        $user_team->user_id = $user_id;
        $user_team->user_accept = (int)(!$this->isExactNumberOfMembers && $user_id == Yii::$app->user->id);
        if (!$user_team->save()) {
            throw new Exception('Ошибка валидации');
        }
    }

    public static function sendInvite($email, $team) {
        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->mailer;
        $mailer->compose(['text' => 'team/send_invite_text'], ['user' => Yii::$app->user->identity, 'team' => $team])
            ->setFrom(Yii::$app->params['email_from'])
            ->setTo($email)
            ->setSubject('Приглашение к участию')
            ->send();
    }

    public function insertUser($data, $team) {
        $this->isExactNumberOfMembers = true;
        $this->numberOfMembers = 1;

        if(!$this->load($data)) {
            return false;
        }

        if(!$user_id = $this->signupUser(0)) {
            return false;
        }

        $this->createUserTeam($user_id, $team);
        $this->sendInvite($this->email0, $team);

        return true;
    }

    /**
     * @param UserTeam $user_team
     * @param array $data
     * @param Team $team
     * @return bool
     */
    public function replaceUser($user_team, $data, $team) {
        $this->isExactNumberOfMembers = true;
        $this->numberOfMembers = 1;

        if(!$this->load($data)) {
            return false;
        }

        if(!$user_id = $this->signupUser(0)) {
            return false;
        }

        $user_team->user_id = $user_id;
        $user_team->user_accept = 0;
        $user_team->team_accept = 1;
        if(!$user_team->save()) {
            return false;
        }

        $this->sendInvite($this->email0, $team);

        return true;
    }

    /**
     * @param integer $i
     * @return array
     */
    public function signupUser($i)
    {
        $passport = PassportApi::passportSearch([
            'email' => $this->{'email' . $i}
        ]);
        if ($passport['result'] == 'success') {
            if (!User::getFromAuth($passport['passports'][0]['user_id'], $passport)) {
                $this->addError('email' . $i, 'Ошибка при создании пользователя');
            } else {
                return $passport['passports'][0]['user_id'];
            }
        } else {
            $is_user = AuthApi::userSearch(['login' => $this->{'email' . $i}]);
            if ($is_user['result'] == 'success') {
                $user['user'] = $is_user['users'][0];
            } else {
                $password = Yii::$app->security->generateRandomString(6);
                $user = AuthApi::userSignup([
                    'login' => $this->{'email' . $i},
                    'password' => $password,
                ]);
                if ($user['result'] == 'error') {
                    $this->addError('email' . $i, 'Невозможно создать пользователя с введённой эл. почтой.');
                    return null;
                }

                try {
                    /** @var \yii\swiftmailer\Mailer $mailer */
                    $mailer = Yii::$app->mailer;
                    $mailer->compose(['text' => 'user/registration_text'], ['login' => $this->{'email' . $i}, 'password' => $password])
                        ->setFrom(Yii::$app->params['email_from'])
                        ->setTo($this->{'email' . $i})
                        ->setSubject('Регистрация на сайте CROSSCHALLENGE.RU')
                        ->send();
                } catch (\Exception $e) {
                }
            }

            //@todo подтверждение эл. почты
            $passport = PassportApi::passportCreate([
                'first_name' => $this->{'first_name' . $i},
                'last_name' => $this->{'last_name' . $i},
                'email' => $this->{'email' . $i},
                'sex' => $this->{'sex' . $i},
                'user_id' => $is_user['users'][0]['id'],
            ]);
            if ($passport['result'] == 'success') {
                if (!User::getFromAuth($user['user']['id'], $passport)) {
                    $this->addError('email' . $i, 'Ошибка при создании пользователя');
                } else {
                    return $user['user']['id'];
                }
            } else {
                AuthApi::userDelete($user['user']['id']);
                $errors = [
                    ApiController::ERROR_ILLEGAL_PASSPORT_FIRSTNAME => 'first_name' . $i,
                    ApiController::ERROR_ILLEGAL_PASSPORT_LASTNAME => 'last_name' . $i,
                    ApiController::ERROR_ILLEGAL_PASSPORT_EMAIL => 'email' . $i,
                    ApiController::ERROR_ILLEGAL_PASSPORT_SEX => 'sex' . $i,
                ];
                foreach ($passport['errors'] as $error) {
                    if (isset($errors[$error['code']])) {
                        $this->addError($errors[$error['code']], $error['title']);
                    } else {
                        $this->addError('email' . $i, ApiController::$_errors[ApiController::ERROR_UNKNOWN]);
                    }
                }
            }
        }
        return null;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'program_id' => 'Программа',
            'email0' => 'Эл. почта',
            'email1' => 'Эл. почта',
            'email2' => 'Эл. почта',
            'email3' => 'Эл. почта',
            'email4' => 'Эл. почта',
            'first_name0' => 'Имя',
            'first_name1' => 'Имя',
            'first_name2' => 'Имя',
            'first_name3' => 'Имя',
            'first_name4' => 'Имя',
            'last_name0' => 'Фамилия',
            'last_name1' => 'Фамилия',
            'last_name2' => 'Фамилия',
            'last_name3' => 'Фамилия',
            'last_name4' => 'Фамилия',
            'sex0' => 'Пол',
            'sex1' => 'Пол',
            'sex2' => 'Пол',
            'sex3' => 'Пол',
            'sex4' => 'Пол',
        ];
    }
}
