<?php
namespace app\models\forms;

use app\models\MiniLeague;
use app\models\UserMiniLeague;
use general\ext\api\passport\PassportApi;
use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * Team form
 */
class MiniLeagueForm extends Model
{
    public $name;
    public $emails;

    private $_passports = [];

    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'string', 'min' => 4, 'max' => 50],
            ['emails', 'each', 'rule' => ['trim']],
            ['emails', 'filter', 'filter' => function($value) {
                //Удалить пустышки
                foreach ($value as $key => $email) {
                    if(empty($email)) {
                        unset($value[ $key ]);
                    }
                }
                return array_values($value);
            }],
            ['emails', 'each', 'rule' => ['email']],
            ['emails', 'emailsCheck'],
        ];
    }

    public function emailsCheck($attribute, $params)
    {
        //Уникальны ли почты?
        $unique_emails = array_unique($this->emails, SORT_STRING);
        if (count($this->emails) != count($unique_emails)) {
            $this->addError('emails', 'Присутствуют повторяющиеся эл. адреса.');
            return;
        }

        //Зарегистрирован ли e-mail
        foreach ($this->emails as $key => $email) {
            $passport = PassportApi::passportSearch([
                'email' => $email
            ]);
            if ($passport['result'] == 'error') {
                $this->addError('emails', 'Присутствуют незарегистрированные эл. адреса.');
                return;
            } else {
                if($passport['passports'][0]['user_id'] != Yii::$app->user->id) {
                    $this->_passports[ $email ] = $passport;
                } else {
                    unset($this->emails[ $key ]);
                }
            }
        }

        if($this->_passports === []) {
            $this->addError('emails', 'Необходимо заполнить.');
        }
    }

    /**
     * Создание лиги
     * @return MiniLeague|null
     * @throws Exception
     */
    public function create()
    {
        if ($this->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $mini_league = new MiniLeague();
                $mini_league->name = $this->name;
                $mini_league->owner_id = Yii::$app->user->id;
                if (!$mini_league->save()) {
                    throw new Exception('Ошибка валидации');
                }

                $user_mini_league = new UserMiniLeague();
                $user_mini_league->mini_league_id = $mini_league->id;
                $user_mini_league->user_id = Yii::$app->user->id;
                $user_mini_league->accepted = 1;
                $user_mini_league->compare_user_id = reset($this->_passports)['passports'][0]['user_id'];
                if (!$user_mini_league->save()) {
                    throw new Exception('Ошибка валидации');
                }

                /** @var \yii\swiftmailer\Mailer $mailer */
                $mailer = Yii::$app->mailer;
                foreach ($this->_passports as $passport) {
                    $user_mini_league = new UserMiniLeague();
                    $user_mini_league->mini_league_id = $mini_league->id;
                    $user_mini_league->user_id = $passport['passports'][0]['user_id'];
                    $user_mini_league->compare_user_id = Yii::$app->user->id;
                    if (!$user_mini_league->save()) {
                        throw new Exception('Ошибка валидации');
                    }
                    try {
                        $mailer->compose(['text' => 'mini-league/send_invite_text'], ['user' => Yii::$app->user->identity, 'mini_league' => $mini_league])
                            ->setFrom(Yii::$app->params['email_from'])
                            ->setTo($passport['passports'][0]['email'])
                            ->setSubject('Приглашение вступить в мини-лигу')
                            ->send();
                    } catch (\Exception $e) {
                    }
                }
                $transaction->commit();

                return $mini_league;
            } catch (Exception $e) {
                $this->addError('name', 'Ошибка при работе с базой данных.');
                $transaction->rollBack();
                //throw $e;
                return null;
            }
        }
        return null;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'emails' => 'Эл. почта',
        ];
    }
}
