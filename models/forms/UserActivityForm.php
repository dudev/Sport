<?php

namespace app\models\forms;

use app\models\ActivityVariant;
use app\models\Program;
use app\models\UserActivityVariant;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\db\Expression;

/**
 * LoginForm is the model behind the login form.
 */
class UserActivityForm extends Model
{
	public $date;
    public $value;

    public function rules() {
        return [
            ['date', 'required'],
            [
                'date',
                'date',
                'max' => date('d.m.Y'),
                'format' => 'dd.MM.yyyy',
                'timestampAttribute' => 'date',
                'timestampAttributeFormat' => 'yyyy-MM-dd',
                'timestampAttributeTimeZone' => 'Asia/Yekaterinburg',
            ],
	        ['date', 'checkDate'],
	        ['value', 'checkValues'],
        ];
    }

	public function checkDate($attribute, $params) {
        $program = Program::find()
            ->where(['<=', 'program.start_date', $this->$attribute])
            ->andWhere(['>', 'program.start_date', new Expression('\'' . $this->$attribute . '\' - INTERVAL program.duration DAY')])
            ->andWhere([
	            'user_team.user_id' => Yii::$app->user->id,
	            'user_team.team_accept' => 1,
	            'user_team.user_accept' => 1,
                //'user_team.is_paid' => 1,
            ])
			//@todo включить проверку оплаты
            ->innerJoinWith('teams.userTeams', false)
            ->exists();
        if(!$program) {
            $this->addError('date', 'В этот день вы не участвуете ни в одной программе');
        }
		$user_activity = UserActivityVariant::find()
			->where([
				'date' => $this->$attribute,
				'user_id' => Yii::$app->user->id,
			])
			->exists();
		if($user_activity) {
			$this->addError($attribute, 'Вы заполнили этот день ранее');
		}
	}

	public function checkValues($attribute, $params) {
		if(is_array($this->$attribute)) {
			if(isset($this->{$attribute}[0])){
                unset($this->{$attribute}[0]);
			}

			foreach ($this->$attribute as $k => $v) {
				if(empty($v)) {
					unset($this->{$attribute}[$k]);
					continue;
				}
				if(!is_numeric($v)) {
					$this->addError('value', 'Проверьте правильность ввода значений активностей');
					return;
				}
                $activity = ActivityVariant::find()
                    ->where(['<=', 'program.start_date', $this->date])
                    ->andWhere(['>', 'program.start_date', new Expression('\'' . $this->date . '\' - INTERVAL program.duration DAY')])
                    ->andWhere([
                        'user_team.user_id' => Yii::$app->user->id,
	                    'user_team.team_accept' => 1,
	                    'user_team.user_accept' => 1,
                        //'user_team.is_paid' => 1,
                        'activity_variant.id' => $k,
                        'activity_variant.sex' => (int)Yii::$app->user->identity->sex,
                    ])
                    //@todo включить проверку оплаты
                    ->innerJoinWith('programActivityVariants.program.teams.userTeams', false)
                    ->exists();
				if(!$activity)  {
					$this->addError($attribute, 'Неизвестная ошибка (1)');
					return;
				}
			}

			if(count($this->$attribute) == 0) {
				$this->addError($attribute, 'Введите значения активностей');
			}
		} else {
			$this->addError($attribute, 'Введите значения активностей');
		}
	}

    public function attributeLabels()
    {
        return [
            'date' => 'Дата',
        ];
    }

	public function save($runValidation = true)
	{
		if($runValidation) {
			if(!$this->validate()) {
				return false;
			}
		}

        $avg = UserActivityVariant::find()
            ->select([new Expression('AVG(user_activity_variant.value) as avg'), 'activity_variant.id as id'])
            ->where([
                'user_team.user_id' => Yii::$app->user->id,
                'activity_variant.id' => array_keys($this->value),
            ])
            ->andWhere(['<=', 'program.start_date', new Expression('NOW()')])
            ->andWhere(['>', 'program.start_date', new Expression('NOW() - INTERVAL program.duration DAY')])
            ->andWhere(['<', 'user_activity_variant.date', $this->date])
            ->innerJoinWith('activityVariant.programs.teams.userTeams', false)
            ->groupBy('activity_variant.id')
            ->indexBy('id')
            ->asArray()
            ->column();

		$transaction = UserActivityVariant::getDb()->beginTransaction();
		try {
			foreach ($this->value as $k => $v) {
				$model = new UserActivityVariant();
				$model->user_id = Yii::$app->user->id;
				$model->date = $this->date;
				$model->activity_variant_id = $k;
                $model->value = $v;
				$model->team_value = intval(isset($avg[$k]) ? ($avg[$k] * 1.1 >= $v ? $v : $avg[$k] * 1.1) : $v);
				if (!$model->save()) {
					throw new Exception('Ошибка валидации');
				}
			}
			$transaction->commit();
            return true;
		} catch(Exception $e) {
			$this->addError('db', 'Ошибка базы данных');
			$transaction->rollBack();
			return false;
		}
	}
}
