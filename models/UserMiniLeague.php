<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_mini_league".
 *
 * @property integer $mini_league_id
 * @property integer $user_id
 * @property integer $compare_user_id
 * @property integer $accepted
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property MiniLeague $miniLeague
 * @property User $user
 */
class UserMiniLeague extends ActiveRecord
{
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_mini_league';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mini_league_id', 'user_id', 'compare_user_id'], 'required'],
            [['mini_league_id', 'user_id', 'compare_user_id'], 'integer'],
            ['accepted', 'default', 'value' => 0],
            ['accepted', 'boolean'],
            [['mini_league_id', 'user_id'], 'unique', 'targetAttribute' => ['mini_league_id', 'user_id'], 'message' => 'The combination of Mini League ID and User ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mini_league_id' => 'Mini League ID',
            'user_id' => 'User ID',
            'compare_user_id' => 'Compare User ID',
            'accepted' => 'Accepted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiniLeague()
    {
        return $this->hasOne(MiniLeague::className(), ['id' => 'mini_league_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompareUser()
    {
        return $this->hasOne(User::className(), ['id' => 'compare_user_id']);
    }
}
