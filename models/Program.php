<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "program".
 *
 * @property integer $id
 * @property string $start_date
 * @property integer $duration
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Team[] $teams
 */
class Program extends ActiveRecord
{
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'duration', 'name'], 'required'],
            [['start_date'], 'safe'],
            [['duration'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'start_date' => 'Дата старта',
            'duration' => 'Продолжительность, дней',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)) {
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        $this->start_date = strtotime($this->start_date);
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(Team::className(), ['program_id' => 'id']);
    }

	public static function getNextProgram() {
		$data = static::find()
			->select(['name', 'id'])
            //@todo включить фильтр
			//->where(['>', 'start_date', new Expression('NOW()')])
			->indexBy('id')
			->column();
		return $data;
	}

    public static function getPrograms() {
        return self::find()
            ->select(['name', 'id'])
            ->indexBy('id')
            ->asArray()
            ->column();
    }
}
