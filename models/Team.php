<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "team".
 *
 * @property integer $id
 * @property string $name
 * @property integer $program_id
 * @property integer $in_search
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $captain
 * @property integer $is_completed
 *
 * @property User $captainUser
 * @property Program $program
 * @property UserTeam[] $userTeams
 * @property User[] $users
 */
class Team extends ActiveRecord
{
    const USER_ACCEPT = 1;
    const USER_REJECT = 2;

    const MANAGE_DELETE_MEMBER_SUCCESS = 0;
    const MANAGE_DELETE_MEMBER_ERROR = 1;
    const MANAGE_REPEAT_SUCCESS = 2;
    const MANAGE_REPEAT_ERROR = 3;
    const MANAGE_REPLACE_USER_SUCCESS = 4;
    const MANAGE_REPLACE_USER_ERROR = 5;
    const MANAGE_REPLACE_CAPTAIN_SUCCESS = 6;
    const MANAGE_REPLACE_CAPTAIN_ERROR = 7;
    const MANAGE_INSERT_USER_SUCCESS = 8;
    const MANAGE_INSERT_USER_ERROR = 9;
    const MANAGE_SEND_MESSAGE_SUCCESS = 10;
    const MANAGE_SEND_MESSAGE_ERROR = 11;

    public static $messages = [
        self::USER_ACCEPT => 'Вы приняли приглашение',
        self::USER_REJECT => 'Вы отклонили приглашение',
        self::MANAGE_DELETE_MEMBER_SUCCESS => 'Участник исключён',
        self::MANAGE_DELETE_MEMBER_ERROR => 'Произошла ошибка. Участник не исключён',
        self::MANAGE_REPEAT_SUCCESS => 'Повторное уведомление отправлено',
        self::MANAGE_REPEAT_ERROR => 'Произошла ошибка. Повторное уведомление не отправлено',
        self::MANAGE_REPLACE_USER_SUCCESS => 'Участник заменён',
        self::MANAGE_REPLACE_USER_ERROR => 'Произошла ошибка. Участник не заменён',
        self::MANAGE_REPLACE_CAPTAIN_SUCCESS => 'Капитан заменён',
        self::MANAGE_REPLACE_CAPTAIN_ERROR => 'Произошла ошибка. Капитан не заменён',
        self::MANAGE_INSERT_USER_SUCCESS => 'Участник добавлен',
        self::MANAGE_INSERT_USER_ERROR => 'Произошла ошибка. Участник не добавлен',
        self::MANAGE_SEND_MESSAGE_SUCCESS => 'Корреспонденция отправлена',
        self::MANAGE_SEND_MESSAGE_ERROR => 'Произошла ошибка. Корреспонденцияне отправлена',
    ];

    public static function getMessage($code)
    {
        return isset(self::$messages[$code]) ? self::$messages[$code] : 'Непредвиденное сообщение';
    }
    
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'program_id'], 'required'],
            [['program_id', 'in_search', 'captain'], 'integer'],
            [['name'], 'string', 'max' => 50],
            ['in_search', 'default', 'value' => 0],
            ['is_completed', 'default', 'value' => 0],
            ['is_completed', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'program_id' => 'Program ID',
            'in_search' => 'In Search',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'captain' => 'Captain',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaptainUser()
    {
        return $this->hasOne(User::className(), ['id' => 'captain']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(Program::className(), ['id' => 'program_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTeams()
    {
        return $this->hasMany(UserTeam::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_team', ['team_id' => 'id']);
    }
}
