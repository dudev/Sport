<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_activity_variant".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property integer $activity_variant_id
 * @property integer $value
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $team_value
 *
 * @property ActivityVariant $activityVariant
 * @property User $user
 */
class UserActivityVariant extends ActiveRecord
{
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_activity_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'date', 'activity_variant_id', 'value', 'team_value'], 'required'],
            [['user_id', 'activity_variant_id', 'value', 'team_value'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'date' => 'Date',
            'activity_variant_id' => 'Activity Variant ID',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'team_value' => 'Team Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityVariant()
    {
        return $this->hasOne(ActivityVariant::className(), ['id' => 'activity_variant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    //среднее кол-во шагов и максимальное кол-во шагов за 30 дней
    //SELECT SUM(ua.value * a.coefficient) FROM `user_activity` as `ua`, `activity` as `a` WHERE ua.activity_id = a.id GROUP BY ua.date ORDER BY ua.date DESC LIMIT 30

    //сумма шагов по каждому виду активности
    //SELECT SUM(ua.value * a.coefficient), a.id FROM `user_activity` as `ua`, `activity` as `a` WHERE ua.activity_id = a.id GROUP BY a.id ORDER BY ua.date DESC LIMIT 30

    //для графика
    //SELECT SUM(ua.value * a.coefficient), a.id, ua.date FROM `user_activity` as `ua`, `activity` as `a` WHERE ua.activity_id = a.id and ua.date > NOW() - INTERVAL 1 MONTH GROUP BY a.id, ua.date
}
