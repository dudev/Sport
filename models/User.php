<?php

namespace app\models;

use general\ext\api\auth\AuthApi;
use general\ext\api\passport\PassportApi;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Command;
use yii\db\Query;
use yii\web\IdentityInterface;

/**
 * @property integer $id
 * @property string $nick
 * @property string $city
 * @property integer $sex
 * @property integer $kind_of_work
 * @property integer $mode_of_work
 * @property integer $change_of_time_zones
 * @property integer $privacy
 */
class User extends ActiveRecord implements IdentityInterface
{
    const PATH_TO_AVATARS = '/avatars/'; //относительно корня сайта (@webroot)

    const KIND_OF_WORK_LOW = 0;
    const KIND_OF_WORK_HIGH = 1;

    const MODE_OF_WORK_STANDARD = 0;
    const MODE_OF_WORK_AFTERNOON_AND_EVENING = 1;
    const MODE_OF_WORK_WITH_NIGHT = 2;
    const MODE_OF_WORK_NIGHT = 3;

    const CHANGE_OF_TIME_ZONES_NO = 0;
    const CHANGE_OF_TIME_ZONES_SOMETIMES = 1;
    const CHANGE_OF_TIME_ZONES_YES = 2;

    const PRIVACY_CLOSE = 0;
    const PRIVACY_GENERAL = 1;
    const PRIVACY_OPEN = 2;

    public static $kind_of_work = [
        User::KIND_OF_WORK_LOW => 'в основном сидячая',
        User::KIND_OF_WORK_HIGH => 'в основном связана с высокой физической активностью',
    ];

    public static $mode_of_work = [
        User::MODE_OF_WORK_STANDARD => 'стандартные часы работы (например, с 9 до 18)',
        User::MODE_OF_WORK_AFTERNOON_AND_EVENING => 'посменно в дневное и вечернее время',
        User::MODE_OF_WORK_WITH_NIGHT => 'посменно, в том числе и в ночные смены',
        User::MODE_OF_WORK_NIGHT => 'только в ночные смены',
    ];

    public static $change_of_time_zones = [
        User::CHANGE_OF_TIME_ZONES_NO => 'никогда',
        User::CHANGE_OF_TIME_ZONES_SOMETIMES => 'иногда',
        User::CHANGE_OF_TIME_ZONES_YES => 'регулярно',
    ];

    public static $privacy = [
        User::PRIVACY_CLOSE => 'закрытый',
        User::PRIVACY_GENERAL => 'открытый (основная информация)',
        User::PRIVACY_OPEN => 'открытый',
    ];

    const MANAGE_SEND_MESSAGE_SUCCESS = 0;
    const MANAGE_SEND_MESSAGE_ERROR = 1;
    const EDIT_USER_SUCCESS = 2;

    public static $messages = [
        self::MANAGE_SEND_MESSAGE_SUCCESS => 'Корреспонденция отправлена',
        self::MANAGE_SEND_MESSAGE_ERROR => 'Произошла ошибка. Корреспонденция не отправлена',
        self::EDIT_USER_SUCCESS => 'Профиль пользователя изменён',
    ];

    public static function getMessage($code)
    {
        return isset(self::$messages[$code]) ? self::$messages[$code] : 'Непредвиденное сообщение';
    }

    private $_auth_key;
    private static $_identities = [];

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            ['nick', 'string', 'min' => 2, 'max' => 50],
            ['city', 'string'],
            ['sex', 'boolean'],
            [['kind_of_work', 'mode_of_work', 'change_of_time_zones', 'privacy'], 'integer'],
            [['kind_of_work', 'mode_of_work', 'change_of_time_zones'], 'default', 'value' => 0],
            ['privacy', 'default', 'value' => User::PRIVACY_OPEN],
            ['city', 'filter', 'filter' => function ($value) {
                return empty($value) ? null : $value;
            }],
        ];
    }

    public static function findIdentity($id)
    {
        if (array_key_exists($id, static::$_identities)) {
            return static::$_identities[$id];
        }
        return static::$_identities[$id] = static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function logout()
    {
        (new Command(['db' => Yii::$app->db]))
            ->delete(AuthKey::tableName(), [
                'user_id' => Yii::$app->user->id,
                'ip' => Yii::$app->request->getUserIP(),
                'browser' => Yii::$app->request->getUserAgent(),
            ])
            ->execute();
        Yii::$app->user->logout();
    }

    public static function logoutEverywhere()
    {
        (new Command(['db' => Yii::$app->db]))
            ->delete(AuthKey::tableName(), [
                'user_id' => Yii::$app->user->id,
            ])
            ->execute();
        Yii::$app->user->logout();
    }

    public function getId()
    {
        return $this->id;
    }

    public static function getFromAuth($id, $passport = null)
    {
        if ($model = User::findOne($id)) {
            return $model;
        }

        if (!$user = AuthApi::userSearch(['id' => $id])) {
            return null;
        }

        $passport = $passport ?: PassportApi::passportSearch([
            'user_id' => $id
        ]);

        if (isset($passport['passports'])) {
            $passport['result'] = 'success';
            $passport['passport'] = $passport['passports'][0];
        }

        $model = new User();
        $model->id = $user['users'][0]['id'];
        $model->sex = empty($passport['passport']['sex']) ? 0 : $passport['passport']['sex'];
        if (!$passport || $passport['result'] != 'success') {
            $model->nick = $user['users'][0]['login'];
        } else {
            $model->nick = $passport['passport']['first_name'] . ' ' . $passport['passport']['last_name'];
        }
        if ($model->save()) {
            return $model;
        }
        return null;
    }

    public function getAuthKey()
    {
        if ($this->_auth_key) {
            return $this->_auth_key;
        }
        $res = (new Query())
            ->select('auth_key')
            ->from(AuthKey::tableName())
            ->where([
                'user_id' => $this->id,
                'ip' => Yii::$app->request->getUserIP(),
                'browser' => Yii::$app->request->getUserAgent(),
            ])
            ->one();
        if ($res) {
            return $this->_auth_key = $res['auth_key'];
        }
        return false;
    }

    public function validateAuthKey($authKey)
    {
        if ($auth_key = $this->getAuthKey()) {
            return $auth_key === $authKey;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthKeys()
    {
        return $this->hasMany(AuthKey::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaptainTeams()
    {
        return $this->hasMany(Team::className(), ['captain' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserActivityVariants()
    {
        return $this->hasMany(UserActivityVariant::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTeams()
    {
        return $this->hasMany(UserTeam::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(Team::className(), ['id' => 'team_id'])->viaTable('user_team', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMiniLeagues()
    {
        return $this->hasMany(UserMiniLeague::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiniLeagues()
    {
        return $this->hasMany(MiniLeague::className(), ['id' => 'mini_league_id'])->viaTable('user_mini_league', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnerMiniLeagues()
    {
        return $this->hasMany(MiniLeague::className(), ['owner_id' => 'id']);
    }
}
