<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.03.2016
 * Time: 1:31
 */
use general\ext\DateHelper;

/** @var \app\models\User $user */
/** @var \app\models\MiniLeague $mini_league */
?>
<?= $user->nick ?> приглашает вас вступить в мини-лигу "<?= $mini_league->name ?>".

Принять приглашение вы можете по ссылке: http://crosschallenge.ru<?= Yii::$app->urlManager->createUrl('cabinet/mini-league') ?>