<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.03.2016
 * Time: 1:31
 */
use general\ext\DateHelper;

/** @var \app\models\User $user */
/** @var \app\models\Team $team */
?>
<?= $user->nick ?> приглашает вас принять участие в программе "<?= $team->program->name ?>" (с <?= date('j', $team->program->start_date) ?> <?= DateHelper::getMonthMin(date('n', $team->program->start_date)) ?>) в составе команды "<?= $team->name ?>".

Принять приглашение вы можете по ссылке: http://crosschallenge.ru<?= Yii::$app->urlManager->createUrl('cabinet/team') ?>