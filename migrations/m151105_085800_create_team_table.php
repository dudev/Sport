<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_085800_create_team_table extends Migration
{
    public function up()
    {
	    $this->createTable('team', [
		    'id' => Schema::TYPE_PK,
		    'name' => Schema::TYPE_STRING . '(50) NOT NULL',
		    'program_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'in_search' => Schema::TYPE_BOOLEAN . ' DEFAULT 0 NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('program_id_FK_team', 'team', 'program_id', 'program', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m151105_085800_create_team_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
