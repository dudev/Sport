<?php

use yii\db\Schema;
use yii\db\Migration;

class m160307_231620_create_activity_variant extends Migration
{
    public function up()
    {
	    $this->renameTable('activity', 'activity_variant');
	    $this->dropColumn('activity_variant', 'name');
	    $this->dropColumn('activity_variant', 'measure');

	    $this->createTable('activity', [
		    'id' => Schema::TYPE_PK,
		    'name' => Schema::TYPE_STRING . '(255) NOT NULL',
		    'measure' => Schema::TYPE_STRING . '(5) NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);

	    $this->batchInsert('activity', [
		    'id',
		    'name',
		    'measure',
		    'created_at',
		    'updated_at',
	    ], [
		    [1, 'Отжимания', 'раз', time(), time()],
		    [2, 'Подтягивание', 'раз', time(), time()],
		    [3, 'Упражнение на пресс', 'раз', time(), time()],
		    [4, 'Коньковый бег', 'раз', time(), time()],
		    [5, 'Планка', 'минута', time(), time()],
		    [6, 'Burpi', 'раз', time(), time()],
	    ]);

	    $this->addColumn('activity_variant', 'activity_id', Schema::TYPE_INTEGER);

	    $this->update('activity_variant', ['activity_id' => 1], ['id' => [1, 2]]);
	    $this->update('activity_variant', ['activity_id' => 2], ['id' => 3]);
	    $this->update('activity_variant', ['activity_id' => 3], ['id' => [4, 5]]);
	    $this->update('activity_variant', ['activity_id' => 4], ['id' => 6]);
	    $this->update('activity_variant', ['activity_id' => 5], ['id' => [7, 8]]);
	    $this->update('activity_variant', ['activity_id' => 6], ['id' => [9, 10]]);

	    $this->addForeignKey('activity_id_FK_activity_variant', 'activity_variant', 'activity_id', 'activity', 'id', 'CASCADE', 'CASCADE');

	    $this->dropTable('program_activity');
	    $this->createTable('program_activity_variant', [
		    'program_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'activity_variant_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('program_id_FK_program_activity_variant', 'program_activity_variant', 'program_id', 'program', 'id', 'CASCADE', 'CASCADE');
	    $this->createIndex('program_activity_variant_tbl_program_id_activity_variant_id_idx', 'program_activity_variant', ['program_id', 'activity_variant_id'], true);
	    $this->addForeignKey('activity_variant_id_FK_program_activity_variant', 'program_activity_variant', 'activity_variant_id', 'activity_variant', 'id', 'CASCADE', 'CASCADE');

	    $this->dropTable('user_activity');

	    $this->createTable('user_activity_variant', [
		    'id' => Schema::TYPE_PK,
		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'date' => Schema::TYPE_DATE . ' NOT NULL',
		    'activity_variant_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'value' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('user_id_FK_user_activity_variant', 'user_activity_variant', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('activity_variant_id_FK_user_activity_variant', 'user_activity_variant', 'activity_variant_id', 'activity_variant', 'id', 'CASCADE', 'CASCADE');

	    $this->batchInsert('program_activity_variant', [
		    'program_id',
		    'activity_variant_id',
		    'created_at',
		    'updated_at',
	    ], [
		    [1, 1, time(), time()],
		    [1, 2, time(), time()],
		    [1, 3, time(), time()],
		    [1, 4, time(), time()],
		    [1, 5, time(), time()],
		    [1, 6, time(), time()],
		    [1, 7, time(), time()],
		    [1, 8, time(), time()],
		    [1, 9, time(), time()],
		    [1, 10, time(), time()],
	    ]);
    }

    public function down()
    {
        echo "m160307_231620_create_activity_variant cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
