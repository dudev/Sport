<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_085737_create_user_activity_table extends Migration
{
    public function up()
    {
	    $this->createTable('user_activity', [
		    'id' => Schema::TYPE_PK,
		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'date' => Schema::TYPE_DATE . ' NOT NULL',
		    'activity_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'value' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('user_id_FK_user_activity', 'user_activity', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('activity_id_FK_user_activity', 'user_activity', 'activity_id', 'activity', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m151105_085737_create_user_activity_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
