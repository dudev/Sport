<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_085539_create_activity_table extends Migration
{
    public function up()
    {
	    $this->createTable('activity', [
		    'id' => Schema::TYPE_PK,
		    'name' => Schema::TYPE_STRING . '(255) NOT NULL',
		    'measure' => Schema::TYPE_STRING . '(5) NOT NULL',
		    'coefficient' => Schema::TYPE_FLOAT . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
    }

    public function down()
    {
        echo "m151105_085539_create_activity_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
