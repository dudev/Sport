<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_060920_add_captain_field_in_team_tbl extends Migration
{
    public function up()
    {
        $this->addColumn('team', 'captain', Schema::TYPE_INTEGER);
        $this->addForeignKey('captain_FK_team', 'team', 'captain', 'user', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        echo "m160215_060920_add_captain_field_in_team_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
