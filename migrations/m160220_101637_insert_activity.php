<?php

use yii\db\Schema;
use yii\db\Migration;

class m160220_101637_insert_activity extends Migration
{
    public function up()
    {
        $this->batchInsert('activity', [
            'id',
            'name',
            'measure',
            'coefficient',
            'sex',
            'created_at',
            'updated_at',
        ], [
            [1, 'Отжимания', 'раз', 0.8, 0, time(), time()],
            [2, 'Отжимания', 'раз', 0.8, 1, time(), time()],
            [3, 'Подтягивание', 'раз', 1.6, 1, time(), time()],
            [4, 'Упражнение на пресс', 'раз', 0.4, 0, time(), time()],
            [5, 'Упражнение на пресс', 'раз', 0.4, 1, time(), time()],
            [6, 'Коньковый бег', 'раз', 0.3, 0, time(), time()],
            [7, 'Планка', 'минута', 8, 0, time(), time()],
            [8, 'Планка', 'минута', 8, 1, time(), time()],
            [9, 'Burpi', 'раз', 1, 0, time(), time()],
            [10, 'Burpi', 'раз', 1, 1, time(), time()],
        ]);
    }

    public function down()
    {
        echo "m160220_101637_insert_activity cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
