<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_060937_add_sex_field_in_user_tbl extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'sex', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m160215_060937_add_sex_field_in_user_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
