<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_085750_create_program_table extends Migration
{
    public function up()
    {
	    $this->createTable('program', [
		    'id' => Schema::TYPE_PK,
		    'start_date' => Schema::TYPE_DATE . ' NOT NULL',
		    'duration' => Schema::TYPE_SMALLINT . ' NOT NULL',
		    'name' => Schema::TYPE_STRING . '(255) NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
    }

    public function down()
    {
        echo "m151105_085750_create_program_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
