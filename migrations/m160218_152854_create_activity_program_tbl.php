<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_152854_create_activity_program_tbl extends Migration
{
    public function up()
    {
        $this->createTable('program_activity', [
            'program_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'activity_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->addForeignKey('program_id_FK_program_activity', 'program_activity', 'program_id', 'program', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('activity_id_FK_program_activity', 'program_activity', 'activity_id', 'activity', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160218_152854_create_activity_program_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
