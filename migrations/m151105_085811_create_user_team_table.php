<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_085811_create_user_team_table extends Migration
{
    public function up()
    {
	    $this->createTable('user_team', [
		    'team_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'team_accept' => Schema::TYPE_BOOLEAN . ' DEFAULT 1 NOT NULL',
		    'user_accept' => Schema::TYPE_BOOLEAN . ' DEFAULT 1 NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('team_id_FK_user_team', 'user_team', 'team_id', 'team', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('user_id_FK_user_team', 'user_team', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->createIndex('user_team_tbl_team_id_user_id_idx', 'user_team', ['team_id', 'user_id'], true);
    }

    public function down()
    {
        echo "m151105_085811_create_user_team_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
