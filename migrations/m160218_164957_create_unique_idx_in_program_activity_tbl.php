<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_164957_create_unique_idx_in_program_activity_tbl extends Migration
{
    public function up()
    {
        $this->createIndex('program_activity_tbl_program_id_activity_id_idx', 'program_activity', ['program_id', 'activity_id'], true);
    }

    public function down()
    {
        echo "m160218_164957_create_unique_idx_in_program_activity_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
