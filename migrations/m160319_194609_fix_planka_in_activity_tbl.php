<?php

use yii\db\Schema;
use yii\db\Migration;

class m160319_194609_fix_planka_in_activity_tbl extends Migration
{
    public function up()
    {
        $this->alterColumn('activity', 'measure', Schema::TYPE_STRING . '(10) NOT NULL');
        $this->update('activity', [
            'measure' => 'секунд',
        ], [
            'id' => 5,
        ]);
    }

    public function down()
    {
        echo "m160319_194609_fix_planka_in_activity_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
