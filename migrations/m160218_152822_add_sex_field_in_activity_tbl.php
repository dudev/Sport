<?php

use yii\db\Schema;
use yii\db\Migration;

class m160218_152822_add_sex_field_in_activity_tbl extends Migration
{
    public function up()
    {
        $this->addColumn('activity', 'sex', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m160218_152822_add_sex_field_in_activity_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
