<?php

use yii\db\Schema;
use yii\db\Migration;

class m160330_001841_add_is_admin_field_in_user_tbl extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'is_admin', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m160330_001841_add_is_admin_field_in_user_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
